FROM python:3

WORKDIR /tmp

# Install Deps
RUN apt-get update -y \
      && apt-get install -y --no-install-recommends \
            ffmpeg \
      && rm -rf /var/lib/apt/lists/*

# Install Whisper
RUN pip install -U openai-whisper

# Pre-fetch models by trying to transcribe devnull
# it will fail, but it will download the model
RUN whisper --model tiny.en /dev/null || true

WORKDIR /root

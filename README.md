# Docker Whisper

Docker image for Whisper.

## Description

This image contains a full install of Whisper.

## Usage examples

### Basic Usage

The most basic example transcribes an mp3 (`example.mp3`) in your current working directory:

```bash
docker run -i -w "/workdir" -v "./:/workdir" --rm registry.gitlab.com/benyanke/docker-whisper:latest whisper --fp16 False --model tiny.en --output_format txt --task transcribe example.mp3
```

### Models

There are a number of whisper models. This container image contains `tiny.en` for small (about 70mb), fast (32x audio speed on an average cpu) and small memory footprint (about 1G) for english lanauge transcription.

For other options or more information, see [Whisper's repo](https://github.com/openai/whisper). You can use other models by specifying a different `--model` option, but they will just be downloaded at runtime. Larger models
can provide better transcription accuracy, but are slower and more resource intensive.

Models are stored at `/root/.cache/whisper/` in the container. It is recomended, if you don't plan to use the `tiny.en` model included in the container, to bind-mount this volume outside the container
so whisper doesn't need to download the models every time the application starts. For example:

```bash
docker run -i -w "/workdir" -v "$HOME/.cache/whisper:/root/.cache/whisper" -v "./:/workdir" --rm registry.gitlab.com/benyanke/docker-whisper:latest whisper --fp16 False --model tiny.en --output_format txt --task transcribe example.mp3 
```

You can use other models, such as `base` (the next bigger model from `tiny`):

```bash
docker run -i -w "/workdir" -v "$HOME/.cache/whisper:/root/.cache/whisper" -v "./:/workdir" --rm registry.gitlab.com/benyanke/docker-whisper:latest whisper --fp16 False --model base.en --output_format txt --task transcribe example.mp3 
```

See the upstream whisper documentation for all models available.

### Improved Security w/ Airgap

By mounting the models to a directory, you can also improve the security of your container by removing network access. Whisper does require network access to download models if you don't
already have them on disk, but once they are downloaded, it does not require access. To use the airgapped workflow, either run the command you wish to use once with network access to download the model,
or download on your own. Then you can add `--network none` to your docker invocation, for truly offline transcription.

```bash
docker run -i -w "/workdir" --network none -v "$HOME/.cache/whisper:/root/.cache/whisper" -v "./:/workdir" --rm registry.gitlab.com/benyanke/docker-whisper:latest whisper --fp16 False --model tiny.en --output_format txt --task transcribe example.mp3 
```

If you run with `--network none` and encounter network issues, it probably means the model is not downloaded and whisper is trying to download. Whisper does not access the internet
unless you request a model (with `--model [MODEL_NAME]`) that is not on disk (at `/root/.cache/whisper/[modelname].pt`).

Typically this is fixed by following these steps:

 - Ensure the container's `/root/.cache/whisper` is mounted to a volume
 - Run the command you wish to execute without `--network none`
 - Check that the model file now exists in the model volume
 - Run the command again with `--network none`, it should use the model file from the volume.

### Bash Alias

Want to be able to run `whisper` commands like in the whisper documentation? Add the following to your `.bashrc` so you don't need to do the docker run invocation every time.

```bash
alias whisper='docker run -i -w "/workdir" -v "./:/workdir" --rm registry.gitlab.com/benyanke/docker-whisper:latest whisper'
```

Then you can run commands like `whisper --model tiny.en --task transcribe example.mp3`. 

## Container Images

The following images are build by CI:

On tags for releases:
 - `registry.gitlab.com/benyanke/docker-whisper:latest`
 - `registry.gitlab.com/benyanke/docker-whisper:{tag}`

On every push to branches:
 - `registry.gitlab.com/benyanke/docker-whisper:branch_{branchname}`
 - `registry.gitlab.com/benyanke/docker-whisper:commit_{commit_hash}`

Note that the `branch_*` and `commit_*` tags are cleaned up regularly.

## Release Process

Test the `commit_*` image after pushing the commit, and do local testing. If it works, make a git tag
matching the pattern `vX.X.X` where `X` is an integer.
